Goldie Backend with React
=========================

## Developer specific properties
Developer specific properties can be added to `{{projectDirectory}}/profiles/dev/config.{{username}}.properties`.
The file is created automatically by Maven. The properties in that file override properties in `config.properties`.

## Running
Run embedded Jetty server with `mvn jetty:run`. To run the frontend in development mode, consult the ```README.md``` inside the ```frontend``` directory.

## Tests
There are unit tests, API/integration tests and end-to-end tests. The API tests are named *IT.java.

Unit tests are run with `mvn test` and all tests are run with `mvn clean verify -Pall-tests,jsSkip` (using jsSkip profile is optional but tests are run faster).

It's slow to run all tests (and wait for the server to restart every time) when debugging API tests.
Instead it's easier to start the server once with `mvn jetty:run -Pintegration-test` and then run wanted tests with IDE.
However, tests might change the database state, which means that running a test again without server restart
might fail even though everything's actually fine.

## Default user accounts
admin/admin   
user/user

## Database support

To make native UUID work on PG use JDBC parameter: stringtype=unspecified
see. https://github.com/pgjdbc/pgjdbc/issues/247

QueryDSL types for native SQL query are generated manually from target database schema using Maven target querydsl:export

## JVM parameters
To avoid JVM OutOfMemory errors then at least the following JVM options must be applied for Tomcat and Maven:

-Xms384m -Xmx512m -XX:+UseConcMarkSweepGC

Some additional tweaks and more memory are recommended for production use:

-Xms512m -Xmx1024m -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true

## Prerequisites

[Maven](https://maven.apache.org/download.cgi)  
Git ([Git for Windows](https://gitforwindows.org))  
[Node.js](https://nodejs.org)  

## Admin tools

For dynamic REST API documentation login as admin and browse to:
http://localhost:9070/swagger-ui.html

## Notes for Eclipse users

Tested with Eclipse Luna Java EE version

- Import existing Maven project -> Eclipse should open dialog "Setup Maven plugin connectors" and let you install buildhelper connector automatically. Select "resolve later" for antrun.
- Install antrun connector: https://code.google.com/p/nl-mwensveen-m2e-extras/
- Install Tomcat and add Tomcat/lib/jsp-api.jar and servlet-api.jar to java build path / libraries.

## Known issues
