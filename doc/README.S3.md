## Configuring S3

1. Create bucket in S3

2. Create user
  - Open menu from username in upper right hand corner
  - Select Security Crendentials
  - Select Get Started with IAM Users
  - Select Create New Users (e.g. "public")
  - Make note of credentials (see step 4)
  
3. Set bucket permissions for created user
  - Select Permissions / Inline Policies / Create User Policy
  - See [link1](http://blogs.aws.amazon.com/security/post/Tx3VRSWZ6B3SHAV/Writing-IAM-Policies-How-to-grant-access-to-an-Amazon-S3-bucket) and
    [link2](http://docs.aws.amazon.com/AmazonS3/latest/dev/example-bucket-policies.html)
  - "s3:PutObjectAcl" is needed for put requests, too

4. Add bucket name and authorization information to src/main/resources/configuration/aws.properties

5. Change endpoint in src/main/resources/s3service.s3-endpoint if necessary

