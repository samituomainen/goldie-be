package fi.vincit.sql;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * SQPersistentLogin is a Querydsl query type for SQPersistentLogin
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class SQPersistentLogin extends com.querydsl.sql.RelationalPathBase<SQPersistentLogin> {

    private static final long serialVersionUID = 2052143396;

    public static final SQPersistentLogin persistentLogin = new SQPersistentLogin("persistent_login");

    public final NumberPath<Integer> consistencyVersion = createNumber("consistencyVersion", Integer.class);

    public final DateTimePath<java.sql.Timestamp> lastUsed = createDateTime("lastUsed", java.sql.Timestamp.class);

    public final StringPath remoteAddress = createString("remoteAddress");

    public final StringPath series = createString("series");

    public final StringPath userAgent = createString("userAgent");

    public final StringPath username = createString("username");

    public final com.querydsl.sql.PrimaryKey<SQPersistentLogin> persistentLoginPkey = createPrimaryKey(series);

    public SQPersistentLogin(String variable) {
        super(SQPersistentLogin.class, forVariable(variable), "public", "persistent_login");
        addMetadata();
    }

    public SQPersistentLogin(String variable, String schema, String table) {
        super(SQPersistentLogin.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public SQPersistentLogin(Path<? extends SQPersistentLogin> path) {
        super(path.getType(), path.getMetadata(), "public", "persistent_login");
        addMetadata();
    }

    public SQPersistentLogin(PathMetadata metadata) {
        super(SQPersistentLogin.class, metadata, "public", "persistent_login");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(consistencyVersion, ColumnMetadata.named("consistency_version").withIndex(3).ofType(Types.INTEGER).withSize(10).notNull());
        addMetadata(lastUsed, ColumnMetadata.named("last_used").withIndex(4).ofType(Types.TIMESTAMP).withSize(35).withDigits(6).notNull());
        addMetadata(remoteAddress, ColumnMetadata.named("remote_address").withIndex(5).ofType(Types.VARCHAR).withSize(255));
        addMetadata(series, ColumnMetadata.named("series").withIndex(2).ofType(Types.VARCHAR).withSize(255).notNull());
        addMetadata(userAgent, ColumnMetadata.named("user_agent").withIndex(6).ofType(Types.VARCHAR).withSize(2147483647));
        addMetadata(username, ColumnMetadata.named("username").withIndex(1).ofType(Types.VARCHAR).withSize(255));
    }

}

