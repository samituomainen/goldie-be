<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" session="true" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="fragment" content="!">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Vincit Oy"/>

    <spring:eval expression="@runtimeEnvironmentUtil.productionEnvironment" htmlEscape="true" javaScriptEscape="true" var="isProductionEnvironment" />
    <%-- Apply cache busting by revisioning JS/CSS assets using GIT revision (prod) or current timestamp (dev) --%>
    <spring:eval expression="@environment['git.commit.id.abbrev']"  javaScriptEscape="true" var="gitCommitId" />
    <jsp:useBean id="currentDate" class="java.util.Date" />
    <c:set var="rev" value="${fn:length(gitCommitId) > 0 && isProductionEnvironment ? gitCommitId : currentDate.time}"  />
    <c:set var="sourcePrefix" value="${contextPath}/v/${rev}"/>

    <title>Goldie BE React</title>
    <link href="${sourcePrefix}/css/app.css" rel="stylesheet">
</head>
<body>
<div id="app-container"></div>
<noscript>
    <div class="no-js">
        <div class="well">
            <p>Selaimesi ei tue JavaScriptia tai olet poistanut JavaScript tuen käytöstä.</p>
            <p>Tämän sivuston käyttäminen vaatii JavaScript tuen. Jos nykyinen selaimesi ei tue JavaScriptia, lataa jokin alla olevista selaimista.</p>
        </div>

        <div class="well">
            <p>Your browser does not support JavaScript or you have disabled JavaScript support.</p>
            <p>This web-site requires JavaScript support. If your current browser doesn't support JavaScript please download some other browser from the links below.</p>
        </div>

        <ul class="list-unstyled well">
            <li><a href="http://www.google.com/chrome">Google Chrome</a></li>
            <li><a href="http://www.apple.com/safari/">Safari</a></li>
            <li><a href="http://www.mozilla.org/en-US/firefox/new/">Mozilla Firefox</a></li>
        </ul>
    </div>
</noscript>
<div class="wrapper">
    <div class="container">

    </div>
</div>
<footer class="footer">

</footer>

<script type="text/javascript" src="${sourcePrefix}/vendor.js"></script>
<script type="text/javascript" src="${sourcePrefix}/app.js"></script>
</body>
</html>
