package fi.vincit;

import fi.vincit.config.AopConfig;
import fi.vincit.config.AsyncConfig;
import fi.vincit.config.CacheConfig;
import fi.vincit.config.Constants;
import fi.vincit.config.DataSourceConfig;
import fi.vincit.config.HttpClientConfig;
import fi.vincit.config.HttpSessionConfig;
import fi.vincit.config.JPAConfig;
import fi.vincit.config.LiquibaseConfig;
import fi.vincit.config.LocalizationConfig;
import fi.vincit.config.QuartzConfig;
import fi.vincit.config.SecurityConfig;
import fi.vincit.config.postinit.PostInitialize;
import fi.vincit.feature.CreateInitialDatabaseContentFeature;
import fi.vincit.feature.mail.MailContext;
import fi.vincit.util.JCEUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import javax.annotation.Resource;

@Configuration
@Import({
        DataSourceConfig.class,
        LiquibaseConfig.class,
        JPAConfig.class,
        CacheConfig.class,
        LocalizationConfig.class,
        AsyncConfig.class,
        QuartzConfig.class,
        MailContext.class,
        HttpClientConfig.class,
        HttpSessionConfig.class,
        SecurityConfig.class,
        AopConfig.class
})
@PropertySource({
        "classpath:configuration/application.properties",
        "classpath:git.properties"
})
@ComponentScan(Constants.FEATURE_BASE_PACKAGE)
public class GoldieApplicationContext {
    @Resource
    private CreateInitialDatabaseContentFeature initialDatabaseContentFeature;

    @PostInitialize
    public void afterStartup() {
        JCEUtil.removeJavaCryptographyAPIRestrictions();

        initialDatabaseContentFeature.initSampleContent();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public boolean isRunningSpringBasedUnitTests() {
        return false;
    }
}
