package fi.vincit.config;

import fi.vincit.controller.api.PasswordResetApiResource;
import fi.vincit.feature.RuntimeEnvironmentUtil;
import fi.vincit.security.authentication.CustomAuthenticationFailureHandler;
import fi.vincit.security.authentication.CustomAuthenticationSuccessHandler;
import fi.vincit.security.authorization.CustomAccessDeniedHandler;
import fi.vincit.security.csrf.CsrfCookieGeneratorFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.csrf.LazyCsrfTokenRepository;
import org.springframework.security.web.savedrequest.NullRequestCache;

import javax.annotation.Resource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String PATTERN_API = "/api/**";
    private static final String PATTERN_ADMIN_API = "/api/v1/admin/**";
    private static final String PATTERN_TEST_API = "/api/test/**";
    private static final String PATTERN_TEST_SECURE_API = "/api/test/secure/**";
    private static final String PATTERN_REVISION_API = "/api/revision";

    private static final String[] PUBLIC_PATTERNS = new String[]{
            PasswordResetApiResource.URI_FORGOT,
            PasswordResetApiResource.URI_RESET
    };

    // Security filter chain can be skipped completely for ignored patterns
    private static final String[] IGNORED_PATTERS = new String[]{
            "/js/**",
            "/css/**",
            "/fonts/**",
            "/i18n/**",
            "/images/**",
            "/webjars/**",
            "/favicon.ico",
            "/sitemap*.xml*"
    };

    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    @Resource
    private SecurityExpressionHandler<FilterInvocation> webSecurityExpressionHandler;

    @Resource
    private RememberMeServices rememberMeServices;

    @Resource
    private LogoutHandler logoutAuditEventListener;

    @Resource
    private CustomAuthenticationSuccessHandler authenticationSuccessHandler;

    @Resource
    private CustomAuthenticationFailureHandler authenticationFailureHandler;

    @Resource
    private CustomAccessDeniedHandler accessDeniedHandler;

    @Resource
    private CsrfTokenRepository csrfTokenRepository;

    @Bean
    public SecurityExpressionHandler<FilterInvocation> webSecurityExpressionHandler(RoleHierarchy roleHierarchy) {
        final DefaultWebSecurityExpressionHandler handler = new DefaultWebSecurityExpressionHandler();
        handler.setRoleHierarchy(roleHierarchy);
        return handler;
    }

    @Bean
    public CsrfTokenRepository csrfTokenRepository() {
        final HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName(CsrfCookieGeneratorFilter.ANGULAR_CSRF_DEFAULT_HEADER_NAME);
        return repository;
    }

    @Bean
    public HttpStatusEntryPoint authenticationEntryPoint() {
        return new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);
    }

    @Override
    public void configure(WebSecurity web) {
        // Public resources do not pass security filter chain
        web.ignoring().antMatchers(IGNORED_PATTERS);
        web.expressionHandler(webSecurityExpressionHandler);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        final HttpSecurity httpSecurity = http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.NEVER)
                .and()

                .httpBasic()
                .authenticationEntryPoint(authenticationEntryPoint())
                .and()

                .formLogin()
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .and()

                .logout()
                .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.NO_CONTENT))
                .addLogoutHandler(logoutAuditEventListener)
                .and()

                .rememberMe()
                .rememberMeServices(rememberMeServices)
                .and()

                .requestCache()
                .requestCache(new NullRequestCache())
                .and()

                .headers()
                .cacheControl().disable()
                .and()

                .csrf()
                .csrfTokenRepository(new LazyCsrfTokenRepository(csrfTokenRepository))
                .ignoringAntMatchers(PATTERN_TEST_API)
                .and()

                .addFilterAfter(new CsrfCookieGeneratorFilter(), CsrfFilter.class)

                .exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler)
                .authenticationEntryPoint(authenticationEntryPoint())
                .and()

                .authorizeRequests()
                .expressionHandler(webSecurityExpressionHandler)
                .antMatchers(PUBLIC_PATTERNS).permitAll()
                .antMatchers(HttpMethod.GET, "/login", "/").permitAll()
                .antMatchers(PATTERN_ADMIN_API, PATTERN_REVISION_API).hasRole("ADMIN")
                .and();

        // Configure requests related to integration testing
        if (runtimeEnvironmentUtil.isDevelopmentEnvironment() || runtimeEnvironmentUtil.isIntegrationTestEnvironment()) {
            httpSecurity.authorizeRequests()
                    .antMatchers(HttpMethod.GET, "/v2/api-docs").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/swagger-ui.html").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/swagger-resources").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/swagger-resources/**").hasRole("ADMIN")
                    .antMatchers(PATTERN_TEST_SECURE_API).hasRole("REST")
                    .antMatchers(PATTERN_TEST_API).permitAll();
        } else {
            httpSecurity.authorizeRequests()
                    .antMatchers(PATTERN_TEST_API).denyAll();
        }

        httpSecurity.authorizeRequests()
                // Default permissions
                .antMatchers(PATTERN_API).hasAnyRole("REST", "USER")
                // Deny anything else
                .anyRequest().denyAll();

        // API testing with CSRF protection is too complicated.
        if (runtimeEnvironmentUtil.isDevelopmentEnvironment()) {
            http.csrf().disable();
        }
    }
}
