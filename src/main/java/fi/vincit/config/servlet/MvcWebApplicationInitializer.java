package fi.vincit.config.servlet;

import fi.vincit.GoldieApplicationContext;
import fi.vincit.config.Constants;
import fi.vincit.config.SpringDataWebConfig;
import fi.vincit.config.WebMVCConfig;
import fi.vincit.config.WebSecurityConfig;
import fi.vincit.config.properties.OverrideConfigInitializer;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.FrameworkServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

@Order(1)
public class MvcWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    // Limit upload size to 50 MBytes
    public static final long MAX_UPLOAD_SIZE = 50 * 1024 * 1024;

    @Override
    public void onStartup(final ServletContext servletContext) throws ServletException {
        servletContext.setInitParameter("org.eclipse.jetty.servlet.Default.useFileMappedBuffer", "false");
        super.onStartup(servletContext);
    }

    @Override
    protected FrameworkServlet createDispatcherServlet(final WebApplicationContext servletAppContext) {
        final DispatcherServlet dispatcherServlet = new DispatcherServlet(servletAppContext);
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
        return dispatcherServlet;
    }

    @Override
    protected void customizeRegistration(final ServletRegistration.Dynamic registration) {
        registration.setMultipartConfig(new MultipartConfigElement("", MAX_UPLOAD_SIZE, -1, 0));
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
                WebSecurityConfig.class,
                GoldieApplicationContext.class
        };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
                WebMVCConfig.class,
                SpringDataWebConfig.class
        };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected ApplicationContextInitializer<?>[] getRootApplicationContextInitializers() {
        return new ApplicationContextInitializer<?>[]{
                new DefaultSpringProfileApplicationContextInitializer(),
                new OverrideConfigInitializer()
        };
    }

    static class DefaultSpringProfileApplicationContextInitializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            applicationContext.getEnvironment().setDefaultProfiles(Constants.DEFAULT_SPRING_PROFILES);
        }
    }
}