package fi.vincit.config;

import fi.vincit.config.profile.EmbeddedDatabase;
import fi.vincit.config.profile.StandardDatabase;
import fi.vincit.config.quartz.QuartzScheduledJobRegistrar;
import fi.vincit.config.quartz.QuartzSpringBeanJobFactory;
import org.quartz.Scheduler;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

@Configuration
@StandardDatabase
public class QuartzConfig {
    @Resource(name = "quartzProperties")
    private Properties quartzProperties;

    @Configuration
    @StandardDatabase
    static class StandardQuartz {
        @Bean
        public Properties quartzProperties() throws IOException {
            final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
            propertiesFactoryBean.setLocation(new ClassPathResource("configuration/quartz.properties"));
            propertiesFactoryBean.afterPropertiesSet();
            return propertiesFactoryBean.getObject();
        }

    }

    @Configuration
    @EmbeddedDatabase
    static class EmbeddedQuartz extends StandardQuartz {
        @Bean
        public Properties quartzProperties() throws IOException {
            final Properties props = super.quartzProperties();
            props.setProperty("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.StdJDBCDelegate");
            return props;
        }

    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(
            PlatformTransactionManager transactionManager,
            DataSource dataSource,
            JobFactory jobFactory) throws IOException {
        final SchedulerFactoryBean factory = new SchedulerFactoryBean();
        // this allows to update triggers in DB when updating settings in config file:
        factory.setOverwriteExistingJobs(true);
        factory.setDataSource(dataSource);
        factory.setTransactionManager(transactionManager);
        factory.setJobFactory(jobFactory);
        factory.setWaitForJobsToCompleteOnShutdown(true);
        factory.setSchedulerName("appScheduler");
        factory.setQuartzProperties(quartzProperties);

        return factory;
    }

    @Bean
    public QuartzScheduledJobRegistrar quartzScheduledJobRegistrar(Scheduler schedulerFactory) {
        final QuartzScheduledJobRegistrar registrar = new QuartzScheduledJobRegistrar();
        registrar.setScheduler(schedulerFactory);

        return registrar;
    }

    @Bean
    public JobFactory jobFactory(ApplicationContext applicationContext) {
        QuartzSpringBeanJobFactory jobFactory = new QuartzSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }
}
