package fi.vincit.config.properties;

import com.zaxxer.hikari.HikariConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:configuration/db.properties")
public class DataSourceProperties {
    @Value("${db.driver}")
    private String driverClassName;

    @Value("${db.url}")
    private String jdbcUrl;

    @Value("${db.min.connections}")
    private int minimumIdle;

    @Value("${db.max.connections}")
    private int maximumPoolSize;

    @Value("${db.username}")
    private String username;

    @Value("${db.password}")
    private String password;

    public HikariConfig buildHikariConfig() {
        final HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setDriverClassName(driverClassName);
        hikariConfig.setJdbcUrl(jdbcUrl);
        hikariConfig.setMinimumIdle(minimumIdle);
        hikariConfig.setMaximumPoolSize(maximumPoolSize);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);

        return hikariConfig;
    }
}
