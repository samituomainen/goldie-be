package fi.vincit.config.hibernate;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

/**
 * Naming strategy that mimics the old {@link org.hibernate.cfg.ImprovedNamingStrategy}
 * @author Joonas Haapsaari
 */
public class ImprovedPhysicalNamingStrategy extends PhysicalNamingStrategyStandardImpl {

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
        name = super.toPhysicalTableName(name, context);
        return context.getIdentifierHelper().toIdentifier(
                Underscorifier.addUnderscores(name.getText()),
                name.isQuoted()
        );
    }

    @Override
    public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment context) {
        name = super.toPhysicalSequenceName(name, context);
        return context.getIdentifierHelper().toIdentifier(
                Underscorifier.addUnderscores(name.getText()),
                name.isQuoted()
        );
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment context) {
        name = super.toPhysicalColumnName(name, context);
        return context.getIdentifierHelper().toIdentifier(
                Underscorifier.addUnderscores(name.getText()),
                name.isQuoted()
        );
    }

}
