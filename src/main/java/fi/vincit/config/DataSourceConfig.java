package fi.vincit.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import fi.vincit.config.profile.EmbeddedDatabase;
import fi.vincit.config.profile.StandardDatabase;
import fi.vincit.config.properties.DataSourceProperties;
import fi.vincit.feature.RuntimeEnvironmentUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.annotation.Resource;
import javax.sql.DataSource;


@Configuration
@Import(DataSourceProperties.class)
public class DataSourceConfig {
    @Configuration
    @StandardDatabase
    static class StandardDatabaseConfiguration {
        @Resource
        private DataSourceProperties dataSourceProperties;

        @Resource
        private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

        @Bean(destroyMethod = "close")
        public DataSource dataSource() {
            final HikariConfig hikariConfig = dataSourceProperties.buildHikariConfig();

            if (runtimeEnvironmentUtil.isProductionEnvironment()) {
                hikariConfig.setInitializationFailTimeout(-1);
            }

            return new HikariDataSource(hikariConfig);
        }
    }

    @Configuration
    @EmbeddedDatabase
    static class TestDatabaseConfiguration extends StandardDatabaseConfiguration {
        @Override
        @Bean(destroyMethod = "shutdown")
        public DataSource dataSource() {
            return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build();
        }
    }
}
