package fi.vincit.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.config.SpringDataWebConfiguration;

@Configuration
public class SpringDataWebConfig extends SpringDataWebConfiguration {
    @Override
    public PageableHandlerMethodArgumentResolver pageableResolver() {
        PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver(this.sortResolver());
        resolver.setMaxPageSize(10000);

        return resolver;
    }
}
