package fi.vincit.config;

import fi.vincit.config.locale.AngularCookieLocaleResolver;
import fi.vincit.feature.RuntimeEnvironmentUtil;
import java.util.Locale;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpHeaders;

@Configuration
public class LocalizationConfig {
    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    @Bean(name = "messageSource")
    public MessageSource messageSource() {
        // MessageSource configuration for localized messages.
        final ReloadableResourceBundleMessageSource source = new ReloadableResourceBundleMessageSource();

        source.setBasenames("/WEB-INF/i18n/messages", "ValidationMessages");
        source.setUseCodeAsDefaultMessage(true);
        source.setFallbackToSystemLocale(false);
        source.setDefaultEncoding("ISO-8859-1");

        if (runtimeEnvironmentUtil.isDevelopmentEnvironment()) {
            // Check for updates on every refresh, otherwise cache forever
            source.setCacheSeconds(1);
        }

        return source;
    }

    @Bean
    public LocaleResolver localeResolver() {
        final CookieLocaleResolver localeResolver = new AngularCookieLocaleResolver() {
            @Override
            protected Locale determineDefaultLocale(HttpServletRequest request) {
                // When there is no cookie.
                return request.getHeader(HttpHeaders.ACCEPT_LANGUAGE) != null
                        ? request.getLocale()
                        : getDefaultLocale();
            }
        };
        localeResolver.setCookieName("NG_TRANSLATE_LANG_KEY");

        // If there is no cookie and no Accept-Language header, default is used.
        localeResolver.setDefaultLocale(runtimeEnvironmentUtil.getDefaultLocale());

        return localeResolver;
    }
}
