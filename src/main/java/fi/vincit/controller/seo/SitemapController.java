package fi.vincit.controller.seo;

import fi.vincit.feature.common.exception.NotFoundException;
import fi.vincit.feature.seo.dto.SiteMapIndex;
import fi.vincit.feature.seo.dto.SiteMapUrlSet;
import fi.vincit.feature.seo.SitemapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 * Controller for generating sitemap XML files. A sitemap index file is generated if
 * there are one or more services (services implementing {@link fi.vincit.feature.seo.SitemapGeneratorService}
 * for generating sitemap urlset files.
 *
 * In order to make the sitemaps work properly, you'll need to implement a service generating the
 * sitemap urlset XML (or amend to the sample {@link fi.vincit.feature.seo.StaticSitemapGeneratorService}
 * service). In addition, you'll need to add the sitemap index XML file location to your robots.txt -file
 *
 * <pre>
 *     Sitemap: https://my.service.com/sitemap.xml
 * </pre>
 *
 * Note that the sitemap-generation is public functionality, so anyone can make your server generate
 * sitemaps. If the sitemap-generation is a heavy operation, you may want to consider optimizations
 * such as caching the sitemap to a file.
 */
@Controller
public class SitemapController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SitemapController.class);

    @Resource
    private SitemapService sitemapService;

    /**
     * Generates a sitemap index XML for all of the sitemap-generating services
     * @return sitemap index XML
     * @throws NotFoundException if there are no sitemap-generating services
     */
    @RequestMapping(value = "/sitemap.xml", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public SiteMapIndex generateSitemapIndex(final HttpServletRequest request) throws NotFoundException {
        LOGGER.debug("Generating sitemap index");
        return sitemapService.generateSitemapIndex(request);
    }

    /**
     * Generates a sitemap urlset XML file by delegating the generation to the respective sitemap-generating service.
     * @param sitemapName the name of the sitemap service
     * @return sitemap urlset XML
     * @throws NotFoundException thrown if the service with the given name is not found
     */
    @RequestMapping(value = "/sitemap_{name}.xml", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public SiteMapUrlSet generateDynamicSitemap(@PathVariable(value = "name") @NotNull String sitemapName,
                                                final HttpServletRequest request) throws NotFoundException {
        LOGGER.debug("Generating sitemap urlset for sitemap_{}.xml.", sitemapName);
        return sitemapService.generateDynamicSitemap(sitemapName, request);
    }
}
