package fi.vincit.controller.api;

import fi.vincit.feature.account.password.reset.ForgotPasswordDTO;
import fi.vincit.feature.account.password.reset.PasswordResetDTO;
import fi.vincit.feature.account.password.reset.PasswordResetFeature;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
public class PasswordResetApiResource {
    public static final String URI_FORGOT = "/api/v1/password/forgot";
    public static final String URI_RESET = "/api/v1/password/reset";

    @Resource
    private PasswordResetFeature passwordResetFeature;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = URI_FORGOT, method = RequestMethod.POST)
    public void forgotPassword(@RequestBody @Validated ForgotPasswordDTO dto, final HttpServletRequest request) {
        passwordResetFeature.sendPasswordResetEmail(dto.getEmail(), request);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = URI_RESET, method = RequestMethod.POST)
    public void resetPassword(@RequestBody @Validated PasswordResetDTO dto, HttpServletRequest httpServletRequest) {
        // Ensure that a session is created so that automatic login works.
        httpServletRequest.getSession();

        passwordResetFeature.processPasswordReset(dto, httpServletRequest);
    }
}
