package fi.vincit.controller.api;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/validation")
public class ValidationApiResource {

    @RequestMapping(value = "phoneNumber",
            method = RequestMethod.GET,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> read(@RequestParam String phoneNumber,
                                  @RequestParam(defaultValue = "FI") String defaultRegion) {
        try {
            final PhoneNumberUtil util = PhoneNumberUtil.getInstance();
            final Phonenumber.PhoneNumber parsedNumber = util.parse(phoneNumber, defaultRegion);

            if (util.isValidNumber(parsedNumber)) {
                return ResponseEntity.ok(util.format(parsedNumber, PhoneNumberUtil.PhoneNumberFormat.E164));
            }
            return ResponseEntity.badRequest().body("INVALID_NUMBER");

        } catch (NumberParseException e) {
            return ResponseEntity.badRequest().body(e.getErrorType());

        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
