package fi.vincit.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

@Constraint(validatedBy = FinnishBusinessIdValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface FinnishBusinessId {
    String message() default "{fi.vincit.validation.FinnishBusinessId.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    boolean verifyChecksum() default true;
}
