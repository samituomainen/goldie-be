package fi.vincit.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

// ISO-3166 two character country code
@Constraint(validatedBy = ISO3166CountryCodeValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ISO3166CountryCode {
    String message() default "{fi.vincit.common.validation.ISO3166CountryCode.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
