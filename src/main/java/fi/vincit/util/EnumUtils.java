package fi.vincit.util;

import java.util.EnumSet;

public class EnumUtils {

    private EnumUtils() {
        throw new AssertionError();
    }

    public static <E extends Enum<E>> long encodeAsBitVector(final EnumSet<E> enumSet) {
        long result = 0L;

        for (E e : enumSet) {
            result |= encodeAsBitMask(e);
        }

        return result;
    }

    public static <E extends Enum<E>> EnumSet<E> decodeFromBitVector(final Class<E> enumType, long bitVector) {
        final EnumSet<E> enumSet = EnumSet.noneOf(enumType);

        for (E enumConstant : enumType.getEnumConstants()) {
            final long bitMask = encodeAsBitMask(enumConstant);

            if (0 != (bitVector & bitMask)) {
                enumSet.add(enumConstant);
                bitVector -= bitMask;
            }
        }

        if (bitVector > 0) {
            throw new IllegalArgumentException("EnumSet bit vector has unknown elements");
        }

        return enumSet;
    }

    private static <E extends Enum<E>> long encodeAsBitMask(final E enumElement) {
        return 1L << enumElement.ordinal();
    }
}
