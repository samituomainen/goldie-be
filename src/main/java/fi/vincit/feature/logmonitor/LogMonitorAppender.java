package fi.vincit.feature.logmonitor;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

// This class is referenced from logback.xml.
public class LogMonitorAppender extends AppenderBase<ILoggingEvent> {

    @Override
    protected void append(ILoggingEvent event) {
        if (event.getLevel().isGreaterOrEqual(Level.DEBUG)) {
            LogMonitor.addLoggingEvent(event.getTimeStamp(), event.getLoggerName());
        }
    }
}
