package fi.vincit.feature;

import fi.vincit.feature.account.password.change.ChangePasswordService;
import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.account.user.SystemUserRepository;
import fi.vincit.security.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Component
public class CreateInitialDatabaseContentFeature {
    private static final Logger LOG = LoggerFactory.getLogger(CreateInitialDatabaseContentFeature.class);

    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    @Resource
    private SystemUserRepository userRepository;

    @Resource
    private ChangePasswordService changePasswordService;

    @Resource
    private boolean isRunningSpringBasedUnitTests;

    @Transactional
    public void initSampleContent() {
        if (isRunningSpringBasedUnitTests) {
            // Skip sample content generation, because not used. See EmbeddedDatabaseTest#initTest.
            return;
        }
        if (!runtimeEnvironmentUtil.isDevelopmentEnvironment() &&
                !runtimeEnvironmentUtil.isIntegrationTestEnvironment() &&
                !runtimeEnvironmentUtil.isStagingEnvironment()) {
            LOG.info("Sample data generation skipped when not in development environment");
            return;
        }

        if (userRepository.count() > 0) {
            LOG.info("Database seems to be populated already, returning...");
            return;
        }

        LOG.info("Creating sample content...");

        try {
            UserInfo admin = UserInfo.create("admin", -1L, SystemUser.Role.ROLE_ADMIN);
            SecurityContextHolder.getContext().setAuthentication(admin.createAuthentication());

            createDatabase();

        } catch (Exception e) {
            throw new RuntimeException("Error generating sample context: {}", e);

        } finally {
            SecurityContextHolder.clearContext();
        }
    }

    private void createDatabase() {
        // Create default admin user
        createUser("admin", SystemUser.Role.ROLE_ADMIN, "admin");

        // Create default user
        createUser("user", SystemUser.Role.ROLE_USER, "user");

        // Create additional users to demo paging and user search
        for (int i = 1; i < 35; i++) {
            // NOTE: Do not set password, because bcrypt is slow.
            createUser("user" + i, SystemUser.Role.ROLE_USER, null);
        }
    }

    private SystemUser createUser(final String username, final SystemUser.Role role, final String password) {
        final SystemUser user = new SystemUser();
        user.setUsername(username);
        user.setRole(role);
        user.setActive(true);

        if (password != null) {
            changePasswordService.setUserPassword(user, password);
        }

        return userRepository.save(user);
    }
}
