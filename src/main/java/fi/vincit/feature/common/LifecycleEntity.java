package fi.vincit.feature.common;

import fi.vincit.security.UserInfo;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@Access(AccessType.FIELD)
public abstract class LifecycleEntity<T extends Serializable> extends BaseEntity<T> {
    @Embedded
    private EntityLifecycleFields lifecycleFields;

    @Embedded
    private EntityAuditFields auditFields;

    private EntityLifecycleFields getLifecycleFields() {
        if (this.lifecycleFields == null) {
            this.lifecycleFields = new EntityLifecycleFields();
        }
        return lifecycleFields;
    }

    private EntityAuditFields getAuditFields() {
        if (auditFields == null) {
            this.auditFields = new EntityAuditFields();
        }
        return auditFields;
    }

    public Date getCreationTime() {
        return getLifecycleFields().getCreationTime();
    }

    public Date getModificationTime() {
        return getLifecycleFields().getModificationTime();
    }

    public Date getDeletionTime() {
        return getLifecycleFields().getDeletionTime();
    }

    public boolean isDeleted() {
        return getDeletionTime() != null;
    }

    public void softDelete() {
        if (!isDeleted()) {
            getLifecycleFields().setDeletionTime(new Date());
            getAuditFields().setDeletedByUserId(getActiveUserId());
        }
    }

    @PrePersist
    protected void prePersist() {
        final Date now = new Date();
        final Long activeUserId = getActiveUserId();

        getLifecycleFields().setCreationTime(now);
        getLifecycleFields().setModificationTime(now);

        getAuditFields().setCreatedByUserId(activeUserId);
        getAuditFields().setModifiedByUserId(activeUserId);
    }

    @PreUpdate
    void preUpdate() {
        getLifecycleFields().setModificationTime(new Date());
        getAuditFields().setModifiedByUserId(getActiveUserId());
    }

    private static Long getActiveUserId() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {
            if (authentication.getPrincipal() == null) {
                return -2L;

            } else if (authentication.getPrincipal() instanceof UserInfo) {
                final Long userId = UserInfo.extractFrom(authentication).getUserId();

                return userId != null ? userId : -3L;
            }

            return -4L;
        }

        return -1L;
    }
}
