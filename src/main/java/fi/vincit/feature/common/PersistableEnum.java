package fi.vincit.feature.common;

public interface PersistableEnum {
    String getDatabaseValue();
}