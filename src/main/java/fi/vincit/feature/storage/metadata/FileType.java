package fi.vincit.feature.storage.metadata;

import java.nio.file.Path;

// File storage can be customized using file content type
public enum FileType {
    TEST_FOLDER(StorageType.LOCAL_FOLDER),
    TEST_DB(StorageType.LOCAL_DATABASE),
    IMAGE_UPLOAD(StorageType.LOCAL_DATABASE);

    private final StorageType storageType;

    FileType(StorageType storageType) {
        this.storageType = storageType;
    }

    public StorageType storageType() {
        return storageType;
    }

    public String formatFilename(final PersistentFileMetadata metadata) {
        // If there are concurrent transactions saving same file, then first transactions will commit,
        // and all other transactions will rollback and delete the file they saved.
        // If all transactions save their file with same filename,
        // then the successful transaction will refer to a deleted file.
        // To create separate file for each transaction append timestamp to filename.
        return metadata.getId().toString() + "_" + System.currentTimeMillis();
    }

    public Path resolveLocalStorageFolder(final Path storageFolder) {
        return storageFolder.resolve(this.name());
    }
}
