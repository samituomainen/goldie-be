package fi.vincit.feature.account;

import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.account.user.SystemUserRepository;
import fi.vincit.feature.common.BaseEntity;
import fi.vincit.security.UserInfo;
import fi.vincit.security.audit.AuthorizationAuditListener;
import fi.vincit.security.authorization.EntityPermissionEvaluator;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class ActiveUserService {
    @Resource
    private SystemUserRepository userRepository;

    @Resource
    private EntityPermissionEvaluator permissionEvaluator;

    @Resource
    private AuthorizationAuditListener auditListener;

    private AuthenticationTrustResolver authTrustResolver = new AuthenticationTrustResolverImpl();

    public Authentication getAuthentication() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();

        return securityContext != null && isAuthenticated(securityContext.getAuthentication())
                ? securityContext.getAuthentication() : null;
    }

    public boolean isAuthenticated() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        return securityContext != null && securityContext.getAuthentication() != null &&
                isAuthenticated(securityContext.getAuthentication());
    }

    private boolean isAuthenticated(final Authentication authentication) {
        return authentication != null && authentication.getPrincipal() != null
                && !authTrustResolver.isAnonymous(authentication) && authentication.isAuthenticated();
    }

    public Optional<UserInfo> getActiveUserInfo() {
        return Optional.ofNullable(getAuthentication()).map(UserInfo::extractFrom);
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
    public Optional<SystemUser> getActiveUser() {
        return getActiveUserInfo()
                .map(UserInfo::getUserId)
                .map(userRepository::findOne);
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
    public void assertHasPermission(final BaseEntity entity, final Enum<?> permission) {
        final Authentication authentication = getAuthentication();
        final boolean hasPermission = permissionEvaluator.hasPermission(authentication, entity, permission);

        // Audit event
        auditListener.onAccessDecision(hasPermission, permission, entity, authentication);

        if (!hasPermission) {
            throw new AccessDeniedException("PermissionEvaluator returned false");
        }
    }

    @Transactional(readOnly = true, propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
    public boolean checkHasPermission(final BaseEntity entity, final Enum<?> permission) {
        final Authentication authentication = getAuthentication();
        return permissionEvaluator.hasPermission(authentication, entity, permission);
    }

    public void loginWithoutCheck(final SystemUser systemUser) {
        final Authentication authentication = UserInfo.create(systemUser).createAuthentication();

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
