package fi.vincit.feature.account.audit;

import fi.vincit.feature.common.BaseEntity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Access(value = AccessType.FIELD)
public class AccountActivityMessage extends BaseEntity<Long> {
    public enum ActivityType {
        LOGIN_SUCCESS,
        LOGIN_FAILURE,
        LOGOUT,
        PASSWORD_CHANGE,
        PASSWORD_RESET,
        PASSWORD_RESET_REQUESTED
    }

    private Long id;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Date creationTime;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ActivityType activityType;

    //an IPV6 address max length is 39 characters
    @Size(min = 0, max = 39)
    @Column(length = 39)
    private String ipAddress;

    //@Column
    //private String userAgent;

    @Column
    private String username;

    @Column
    private Long userId;

    @Column(columnDefinition = "text")
    private String exceptionMessage;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    @Column(name = "message_id", nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public AccountActivityMessage() {
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String remoteAddr) {
        this.ipAddress = remoteAddr;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    @PrePersist
    protected void prePersist() {
        creationTime = new Date();
    }
}
