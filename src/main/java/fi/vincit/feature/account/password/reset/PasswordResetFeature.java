package fi.vincit.feature.account.password.reset;

import com.github.jknack.handlebars.Handlebars;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import fi.vincit.feature.RuntimeEnvironmentUtil;
import fi.vincit.feature.account.ActiveUserService;
import fi.vincit.feature.account.audit.AccountAuditService;
import fi.vincit.feature.account.password.change.ChangePasswordService;
import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.account.user.SystemUserRepository;
import fi.vincit.feature.mail.MailMessageDTO;
import fi.vincit.feature.mail.MailService;
import fi.vincit.feature.mail.token.EmailToken;
import fi.vincit.feature.mail.token.EmailTokenService;
import fi.vincit.feature.mail.token.EmailTokenType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.session.Session;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Stream;

import static org.springframework.session.FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME;

@Component
public class PasswordResetFeature {
    private static final Logger LOG = LoggerFactory.getLogger(PasswordResetFeature.class);

    public static final String TEMPLATE_PASSWORD_RESET = "email_password_reset";

    @Resource
    private EmailTokenService emailTokenService;

    @Resource
    private ChangePasswordService changePasswordService;

    @Resource
    private ActiveUserService activeUserService;

    @Resource
    private MailService mailService;

    @Resource
    private AccountAuditService accountAuditService;

    @Resource
    private FindByIndexNameSessionRepository<? extends Session> sessionRepository;

    @Resource
    private Handlebars handlebars;

    @Resource
    private MessageSource messageSource;

    @Resource
    private SystemUserRepository userRepository;

    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    /**
     * Sends password renewal link to user(s) specified by email.
     * User can use the link to login to the application and to change her password.
     *
     * @param email Specifies the user(s) who will receive password renewal link.
     */
    @Transactional
    public void sendPasswordResetEmail(final String email, final HttpServletRequest request) {
        LOG.debug("Send password renewal link to '{}'", email);

        // There can be multiple users with the same email
        for (final SystemUser user : userRepository.findByEmail(email)) {
            // Log account activity
            accountAuditService.auditPasswordResetRequest(user, activeUserService.getAuthentication());

            final Locale userLocale = MoreObjects.firstNonNull(user.getLocale(), LocaleContextHolder.getLocale());
            final String subject = messageSource.getMessage("account.password.reset.mail.subject", null, userLocale);

            final URI passwordResetLink = ServletUriComponentsBuilder.fromRequest(request)
                    .replacePath("/password/reset/{token}")
                    .buildAndExpand(emailTokenService.allocateToken(EmailTokenType.PASSWORD_RESET, user, email, request))
                    .toUri();

            final ImmutableMap<String, Object> params = ImmutableMap.<String, Object>of(
                    "link", passwordResetLink.toString());

            mailService.sendImmediate(new MailMessageDTO.Builder()
                    .withTo(user.getEmail())
                    .withSubject(subject)
                    .withHandlebarsBody(handlebars, TEMPLATE_PASSWORD_RESET, params));
        }
    }

    /**
     * Process password renewal token
     *
     * @return forward link stored in token or default redirect
     */
    @Transactional
    public void processPasswordReset(final PasswordResetDTO resetDTO, final HttpServletRequest request) {
        Preconditions.checkNotNull(resetDTO, "No dto");
        Preconditions.checkArgument(StringUtils.hasText(resetDTO.getToken()), "Token is empty");
        Preconditions.checkArgument(StringUtils.hasText(resetDTO.getPassword()), "Empty password");

        final EmailToken emailToken = emailTokenService.validateAndRevoke(resetDTO.getToken(), request);

        final SystemUser systemUser = Objects.requireNonNull(emailToken.getUser());

        // Make sure all existing rememberMe logins are revoked
        findSessionKeysByUsername(systemUser.getUsername()).forEach(sessionRepository::delete);

        if (systemUser != null) {
            if (systemUser.isActive() && !systemUser.isDeleted()) {
                // Log account activity
                accountAuditService.auditPasswordReset(systemUser, activeUserService.getAuthentication());

                changePasswordService.setUserPassword(systemUser, resetDTO.getPassword());

                if (systemUser.getRole() == SystemUser.Role.ROLE_USER) {
                    activeUserService.loginWithoutCheck(systemUser);
                }

            } else {
                LOG.error("Refusing to process password reset for de-active userId={} username={}",
                        systemUser.getId(), systemUser.getUsername());
            }

        } else {
            throw new IllegalStateException("No valid user with password reset token userId");
        }
    }

    private Stream<String> findSessionKeysByUsername(final String username) {
        return sessionRepository.findByIndexNameAndIndexValue(PRINCIPAL_NAME_INDEX_NAME, username).keySet().stream();
    }
}
