package fi.vincit.feature.account;

import fi.vincit.feature.account.audit.AccountAuditService;
import fi.vincit.feature.account.password.change.ChangePasswordDTO;
import fi.vincit.feature.account.password.change.ChangePasswordService;
import fi.vincit.feature.common.exception.NotFoundException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Component
@PreAuthorize("hasRole('ROLE_USER')")
public class AccountEditFeature {
    @Resource
    private ActiveUserService activeUserService;

    @Resource
    private ChangePasswordService changePasswordService;

    @Resource
    private AccountAuditService accountAuditService;

    @Transactional(readOnly = true)
    public AccountDTO getActiveAccount() {
        return activeUserService.getActiveUser()
                .map(AccountDTO::new)
                .orElseThrow(NotFoundException::new);
    }

    @Transactional
    public void updateActiveAccount(final AccountDTO dto) {
        activeUserService.getActiveUser().ifPresent(activeUser -> {
            activeUser.setEmail(dto.getEmail());
            activeUser.setFirstName(dto.getFirstName());
            activeUser.setLastName(dto.getLastName());
            activeUser.setLocale(dto.getLocale());
            activeUser.setTimeZone(dto.getTimeZone());
        });
    }

    @Transactional
    public void changeActiveUserPassword(final ChangePasswordDTO dto) {
        activeUserService.getActiveUser().ifPresent(activeUser -> {
            changePasswordService.setUserPassword(activeUser, dto.getPassword());
            accountAuditService.auditPasswordChange(activeUser, activeUserService.getAuthentication());
        });
    }
}
