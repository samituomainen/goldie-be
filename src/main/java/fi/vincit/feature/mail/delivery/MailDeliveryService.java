package fi.vincit.feature.mail.delivery;

import fi.vincit.feature.mail.MailMessageDTO;

import java.util.Map;
import java.util.Set;

public interface MailDeliveryService {
    void send(MailMessageDTO message);

    void sendAll(Map<Long, MailMessageDTO> outgoingBatch,
                 Set<Long> successfulMessages,
                 Set<Long> failedMessages);
}
