package fi.vincit.feature.mail.delivery;

import fi.vincit.feature.RuntimeEnvironmentUtil;
import fi.vincit.feature.mail.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.IntervalTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component
public class EmailSchedulerJob implements SchedulingConfigurer {
    private static final Logger LOG = LoggerFactory.getLogger(EmailSchedulerJob.class);

    public static final long SEND_MAIL_INTERVAL = TimeUnit.SECONDS.toMillis(30);
    public static final long INITIAL_DELAY = TimeUnit.SECONDS.toMillis(5);

    @Resource
    private MailService mailService;

    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        if (runtimeEnvironmentUtil.isEmailSendingEnabled()) {
            LOG.info("Mail scheduler is enabled");

            taskRegistrar.addFixedDelayTask(
                    new IntervalTask(new MailTaskRunner(),
                            SEND_MAIL_INTERVAL, INITIAL_DELAY));
        } else {
            LOG.info("Mail scheduler is disabled");
        }
    }

    public class MailTaskRunner implements Runnable {
        @Override
        public void run() {
            try {
                mailService.processOutgoingMail();
            } catch (Exception ex) {
                LOG.error("Mail scheduler error!", ex);
            }
        }
    }
}
