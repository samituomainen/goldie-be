package fi.vincit.feature.mail.queue;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;

public interface MailMessageRepository extends JpaRepository<PersistentMailMessage, Long> {
    @Query("SELECT m FROM PersistentMailMessage m WHERE m.delivered IS FALSE AND m.failureCounter < ?1 AND m.scheduledTime < ?2")
    List<PersistentMailMessage> findUnsentMessages(int maxFailures, Instant sentBefore, Pageable page);
}
