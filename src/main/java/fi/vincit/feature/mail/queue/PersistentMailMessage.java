package fi.vincit.feature.mail.queue;

import fi.vincit.feature.common.BaseEntity;

import java.time.Instant;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "mail_message")
@Access(value = AccessType.FIELD)
public class PersistentMailMessage extends BaseEntity<Long> {

    private Long id;

    @NotNull
    @Email
    @Column(nullable = false)
    private String fromEmail;

    @NotNull
    @Email
    @Column(nullable = false)
    private String toEmail;

    @NotBlank
    @Column(nullable = false)
    private String subject;

    @NotBlank
    @Column(columnDefinition = "text", nullable = false)
    private String body;

    @NotNull
    @Column(nullable = false)
    private Instant submitTime;

    @NotNull
    @Column(nullable = false)
    private Instant scheduledTime;

    @Column(nullable = false)
    private boolean delivered;

    @Column
    private Instant deliveryTime;

    @Column
    private Instant lastAttemptTime;

    @Column(nullable = false)
    private int failureCounter;

    @Override
    @Id
    @Access(value = AccessType.PROPERTY)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mail_message_id", nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void markAsDelivered() {
        if (!isDelivered()) {
            setDelivered(true);
            setDeliveryTime(Instant.now());
            setLastAttemptTime(Instant.now());
        } else {
            throw new IllegalStateException("Already delivered id=" + getId());
        }
    }

    public void incrementFailureCounter() {
        if (!isDelivered()) {
            setFailureCounter(getFailureCounter() + 1);
            setLastAttemptTime(Instant.now());
        } else {
            throw new IllegalStateException("Already delivered id=" + getId());
        }
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Instant getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Instant submitTime) {
        this.submitTime = submitTime;
    }

    public Instant getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(Instant scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public Instant getLastAttemptTime() {
        return lastAttemptTime;
    }

    public void setLastAttemptTime(Instant lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public Instant getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Instant deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public int getFailureCounter() {
        return failureCounter;
    }

    public void setFailureCounter(int failureCounter) {
        this.failureCounter = failureCounter;
    }
}
