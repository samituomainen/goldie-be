package fi.vincit.feature.mail.token;

import fi.vincit.feature.common.PersistableEnumConverter;

import javax.persistence.Converter;

@Converter
public class EmailTokenTypeConverter implements PersistableEnumConverter<EmailTokenType> {
}