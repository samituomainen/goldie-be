package fi.vincit.feature.mail.token;

import com.google.common.base.Preconditions;
import com.google.common.collect.Range;
import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.common.BaseEntity;

import javax.annotation.Nonnull;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Objects;
import javax.persistence.JoinColumn;

@Entity
@Access(value = AccessType.FIELD)
public class EmailToken extends BaseEntity<String> {

    private String id;

    @NotNull
    @Column(nullable = false, length = 1)
    @Convert(converter = EmailTokenTypeConverter.class)
    private EmailTokenType tokenType;

    @NotNull
    @Column(nullable = false)
    private Instant validFrom;

    @NotNull
    @Column(nullable = false)
    private Instant validUntil;

    @Column
    private Instant revokedAt;

    @NotBlank
    @Column(nullable = false, length = 255)
    private String createRemoteAddress;

    @Column(length = 255)
    private String revokeRemoteAddress;

    @Email
    @NotNull
    @Column(nullable = false, length = 255)
    private String email;

    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SystemUser user;

    public EmailToken() {
        super();
    }

    public EmailToken(final String tokenData,
                      final SystemUser user,
                      final EmailTokenType tokenType,
                      final Instant validUntil,
                      final String remoteAddress,
                      final String email) {
        this();
        this.id = Objects.requireNonNull(tokenData);
        this.user = user;
        this.tokenType = Objects.requireNonNull(tokenType);
        this.validFrom = Instant.now();
        this.validUntil = Objects.requireNonNull(validUntil);
        this.createRemoteAddress = Objects.requireNonNull(remoteAddress);
        this.email = Objects.requireNonNull(email).trim().toLowerCase();
    }

    @Transient
    public boolean isValid(@Nonnull final Instant now) {
        Objects.requireNonNull(now);
        return Range.closed(getValidFrom(), getValidUntil()).contains(now);
    }

    public void revoke(@Nonnull HttpServletRequest request) {
        Preconditions.checkState(this.revokedAt == null, "Already in revoked state");
        setRevokedAt(Instant.now());
        setRevokeRemoteAddress(request.getRemoteAddr());
    }

    @Transient
    @AssertTrue
    boolean isValidFromUntilOrderingValid() {
        return validFrom == null || validUntil == null || !validFrom.isAfter(validUntil);
    }

    @Override
    @Id
    @Size(min = 16, max = 255)
    @Access(value = AccessType.PROPERTY)
    @Column(name = "token_data", nullable = false, length = 255)
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public EmailTokenType getTokenType() {
        return tokenType;
    }

    public Instant getValidFrom() {
        return validFrom;
    }

    public Instant getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(final Instant validUntil) {
        this.validUntil = validUntil;
    }

    public Instant getRevokedAt() {
        return revokedAt;
    }

    public void setRevokedAt(final Instant revokedAt) {
        this.revokedAt = revokedAt;
    }

    public String getCreateRemoteAddress() {
        return createRemoteAddress;
    }

    public String getRevokeRemoteAddress() {
        return revokeRemoteAddress;
    }

    public void setRevokeRemoteAddress(final String revokeRemoteAddress) {
        this.revokeRemoteAddress = revokeRemoteAddress;
    }

    public String getEmail() {
        return email;
    }

    public SystemUser getUser() {
        return user;
    }
}