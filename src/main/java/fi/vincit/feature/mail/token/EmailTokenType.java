package fi.vincit.feature.mail.token;

import com.google.common.io.BaseEncoding;
import fi.vincit.feature.common.PersistableEnum;
import org.springframework.security.crypto.keygen.BytesKeyGenerator;

import javax.annotation.Nonnull;
import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.Period;

public enum EmailTokenType implements PersistableEnum {
    PASSWORD_RESET("P", Duration.ofHours(2));

    private final String databaseValue;
    private final Duration validityPeriod;

    EmailTokenType(final String databaseValue,
                   final Duration validityPeriod) {
        this.databaseValue = databaseValue;
        this.validityPeriod = validityPeriod;
    }

    @Override
    public String getDatabaseValue() {
        return databaseValue;
    }

    public Instant calculateValidUntil(final @Nonnull Instant now) {
        return now.plus(validityPeriod);
    }

    public boolean isUserRequired() {
        return true;
    }

    public String generateSecureToken(final BytesKeyGenerator pseudoRandomGenerator) {
        final String randomString = BaseEncoding.base64Url().encode(pseudoRandomGenerator.generateKey());
        return cut(randomString, 255);
    }

    private static String cut(final String s, final int maxLength) {
        return s.length() > maxLength ? s.substring(0, maxLength) : s;
    }
}