package fi.vincit.security.csrf;

import fi.vincit.controller.api.AccountApiResource;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter used to put the CSRF token generated by Spring Security in a cookie for use by AngularJS.
 * ref: https://spring.io/blog/2015/01/12/the-login-page-angular-js-and-spring-security-part-ii
 */
public class CsrfCookieGeneratorFilter extends OncePerRequestFilter {
    public static final String ANGULAR_CSRF_DEFAULT_COOKIE_NAME = "XSRF-TOKEN";
    public static final String ANGULAR_CSRF_DEFAULT_HEADER_NAME = "X-XSRF-TOKEN";

    @Override
    protected void doFilterInternal(
            HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if (request.getSession(false) == null &&
                !request.getRequestURI().equals("/") &&
                !request.getRequestURI().equals(AccountApiResource.BASE_URI)) {
            filterChain.doFilter(request, response);
            return;
        }

        final CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class.getName());

        if (csrf != null) {
            Cookie cookie = WebUtils.getCookie(request, ANGULAR_CSRF_DEFAULT_COOKIE_NAME);

            if (cookie == null || csrf.getToken() != null && !csrf.getToken().equals(cookie.getValue())) {
                cookie = new Cookie(ANGULAR_CSRF_DEFAULT_COOKIE_NAME, csrf.getToken());
                cookie.setMaxAge(-1);
                cookie.setPath(getCookiePath(request));
                cookie.setSecure(request.isSecure());
                cookie.setPath(getCookiePath(request));
                response.addCookie(cookie);
            }
        }

        filterChain.doFilter(request, response);
    }

    private static String getCookiePath(final HttpServletRequest request) {
        final String contextPath = request.getContextPath();

        return contextPath.length() > 0 ? contextPath : "/";
    }
}
