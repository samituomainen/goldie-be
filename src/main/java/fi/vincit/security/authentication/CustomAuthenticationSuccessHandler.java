package fi.vincit.security.authentication;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException {

        if (authentication != null && authentication.isAuthenticated()) {
            response.sendRedirect(response.encodeRedirectURL("/api/v1/account"));

        } else {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }
}
