/*
 * Copyright 2012-2016 Jarkko Kaura
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fi.vincit.util;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class ResourceUtils {

    public static final Function<? super MetadataReader, Class<?>> METAREADER_TO_CLASS = reader -> {
        if (reader == null) {
            return null;
        }

        try {
            final String className = reader.getClassMetadata().getClassName();
            return ClassUtils.forName(className, ClassUtils.getDefaultClassLoader());
        } catch (final ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    };
    private static final ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
    private static final MetadataReaderFactory metadataReaderFactory =
            new CachingMetadataReaderFactory(resourcePatternResolver);

    private ResourceUtils() {
        throw new AssertionError();
    }

    @Nonnull
    public static Set<Class<?>> getClasses(@Nonnull final String basePackage, @Nonnull final String pathComponent) {
        return getMetadataReadersInternal(basePackage)
                .filter(getPathComponentPredicate(pathComponent))
                .map(METAREADER_TO_CLASS)
                .collect(Collectors.toSet());
    }

    @Nonnull
    public static List<MetadataReader> getMetadataReaders(@Nonnull final String basePackage) {
        return getMetadataReadersInternal(basePackage).collect(Collectors.toList());
    }

    @Nonnull
    public static List<MetadataReader> getMetadataReaders(
            @Nonnull final String basePackage, @Nonnull final String requiredPathComponent) {

        return getMetadataReadersInternal(basePackage)
                .filter(getPathComponentPredicate(requiredPathComponent))
                .collect(Collectors.toList());
    }

    @Nonnull
    private static Stream<MetadataReader> getMetadataReadersInternal(@Nonnull final String basePackage) {
        Objects.requireNonNull(basePackage, "basePackage must not be null");

        final String packageSearchPath = String.format("classpath*:%s/**/*.class", getResourcePath(basePackage));

        final Function<? super Resource, MetadataReader> func = r -> {
            try {
                return metadataReaderFactory.getMetadataReader(r);
            } catch (final IOException e) {
                throw new RuntimeException(e);
            }
        };

        try {
            final Resource[] resources = resourcePatternResolver.getResources(packageSearchPath);
            return Stream.of(resources).filter(r -> r.isReadable()).map(func);

        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String getResourcePath(final String packageName) {
        return ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(packageName));
    }

    private static final Predicate<? super MetadataReader> getPathComponentPredicate(
            final String requiredPathComponent) {

        Objects.requireNonNull(requiredPathComponent, "requiredPathComponent must not be null");

        return input -> {
            try {
                String path = input.getResource().getFile().getAbsolutePath().toString();
                path = path.replace("\\", "/");
                return path.contains(requiredPathComponent);
            } catch (final IOException e) {
                throw new RuntimeException(e);
            }
        };
    }

}