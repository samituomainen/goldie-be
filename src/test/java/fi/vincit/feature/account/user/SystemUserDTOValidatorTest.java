package fi.vincit.feature.account.user;

import fi.vincit.EmbeddedDatabaseTest;
import javax.annotation.Resource;
import org.junit.Test;
import org.junit.Before;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import org.springframework.security.crypto.password.PasswordEncoder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.springframework.validation.Errors;

public class SystemUserDTOValidatorTest extends EmbeddedDatabaseTest {

    @Resource
    private PasswordEncoder passwordEncoder;
    @Resource
    private SystemUserRepository systemUserRepository;

    private Errors errors;
    private SystemUser user;
    private SystemUserDTOValidator validator;

    @Before
    public void setup() {
        errors = mock(Errors.class);
        validator = new SystemUserDTOValidator(systemUserRepository);

        user = model().newUser("user", SystemUser.Role.ROLE_USER, passwordEncoder);
        persistInNewTransaction();
    }

    @Test
    public void testCreateNewUserWhenUsernameExists() {
        SystemUserDTO dto = createDTO("user", null);
        validator.validate(dto, errors);
        verifyFieldError("username", 1);
    }

    @Test
    public void testUpdateUserKeepUsername() {
        SystemUserDTO dto = createDTO("user", user.getId());
        validator.validate(dto, errors);
        verifyFieldError("username", 0);
    }

    @Test
    public void testUpdateUserChangeUsername() {
        SystemUserDTO dto = createDTO("user", 0L);
        validator.validate(dto, errors);
        verifyFieldError("username", 1);
    }

    private SystemUserDTO createDTO(String username, Long id) {
        SystemUserDTO dto = new SystemUserDTO();
        dto.setUsername(username);
        dto.setId(id);
        return dto;
    }

    private void verifyFieldError(String fieldName, int wantedNumberOfInvocations) {
        verify(errors, times(wantedNumberOfInvocations)).rejectValue(eq(fieldName), any(), any());
    }
}
