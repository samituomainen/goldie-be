package fi.vincit.controller.api.user;

import com.jayway.restassured.http.ContentType;
import fi.vincit.controller.api.AbstractRestApiIT;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.HttpStatus;

public class RestUserListIT extends AbstractRestApiIT {
    @Test
    public void testListAll() {
        givenAdminAuthentication()
                .expect()
                .statusCode(HttpStatus.OK.value())
                .contentType(ContentType.JSON)
                .body("findall", Matchers.not(Matchers.empty()))
                .rootPath("findall")
                .body(
                        "id", Matchers.notNullValue(),
                        "username", Matchers.not(Matchers.isEmptyOrNullString()),
                        "active", Matchers.notNullValue(),
                        "role", Matchers.not(Matchers.isEmptyOrNullString()),
                        "rev", Matchers.notNullValue())
                .when()
                .get(Constants.API_PREFIX);
    }

    @Test
    public void testPageResults() {
        givenAdminAuthentication()
                .param("type", "page")
                .expect()
                .statusCode(HttpStatus.OK.value())
                .contentType(ContentType.JSON)
                .body("content.findall", Matchers.not(Matchers.empty()))
                .when()
                .get(Constants.API_PREFIX);
    }
}
