package fi.vincit.controller.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.filter.log.RequestLoggingFilter;
import com.jayway.restassured.filter.log.ResponseLoggingFilter;
import com.jayway.restassured.parsing.Parser;
import com.jayway.restassured.specification.RequestSpecification;
import fi.vincit.config.jackson.CustomJacksonObjectMapper;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.io.IOException;

import static com.jayway.restassured.RestAssured.given;

@RunWith(BlockJUnit4ClassRunner.class)
public abstract class AbstractRestApiIT {

    protected static final String API_USERNAME = "admin";
    protected static final String API_PASSWORD = "admin";
    protected static ObjectMapper OBJECT_MAPPER = new CustomJacksonObjectMapper(true);

    @BeforeClass
    public static void init() throws IOException {
        RestAssured.reset();
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 9070;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(
                new ObjectMapperConfig().jackson2ObjectMapperFactory((aClass, s) -> OBJECT_MAPPER));
    }

    protected static RequestSpecification givenAdminAuthentication() {
        return given().auth().preemptive().basic(API_USERNAME, API_PASSWORD);
    }
}
