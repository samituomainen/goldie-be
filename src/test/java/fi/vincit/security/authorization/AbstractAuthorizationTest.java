package fi.vincit.security.authorization;

import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.common.BaseEntity;
import fi.vincit.security.EntityPermission;
import fi.vincit.security.UserInfo;
import fi.vincit.security.authentication.TestAuthenticationTokenUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.test.util.ReflectionTestUtils;

import javax.annotation.Nonnull;

import static org.assertj.core.api.Assertions.assertThat;

public class AbstractAuthorizationTest {
    public enum Permission {
        OTHER,
        UNKNOWN;
    }

    public enum AuthorizationRole {
        ROLE_ENTITY;
    }

    private static class SimpleEntity extends BaseEntity<Long> {
        private Long id = 1L;

        @Override
        public Long getId() {
            return id;
        }

        @Override
        public void setId(Long id) {
            this.id = id;
        }
    }

    static class SimpleAuthorizationStrategy extends AbstractEntityAuthorization<SimpleEntity> {
        SimpleAuthorizationStrategy() {
            allow(EntityPermission.READ, SystemUser.Role.ROLE_USER);
            allow(EntityPermission.UPDATE, SystemUser.Role.ROLE_ADMIN);
            allow(Permission.OTHER, AuthorizationRole.ROLE_ENTITY);
        }

        @Override
        protected void authorizeTarget(@Nonnull final AuthorizationTokenCollector collector,
                                       @Nonnull final SimpleEntity entity,
                                       @Nonnull final UserInfo userInfo) {
            collector.addAuthorizationRole(AuthorizationRole.ROLE_ENTITY);
        }
    }

    @Mock
    private RoleHierarchy roleHierarchy;

    private EntityAuthorizationStrategy<SimpleEntity> strategy;

    private SimpleEntity entity;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.entity = new SimpleEntity();
        this.strategy = new SimpleAuthorizationStrategy();

        Mockito.when(roleHierarchy.getReachableGrantedAuthorities(Mockito.anyCollectionOf(GrantedAuthority.class)))
                .thenAnswer(invocationOnMock -> invocationOnMock.getArguments()[0]);

        ReflectionTestUtils.setField(strategy, "roleHierarchy", roleHierarchy);
    }

    @Test
    public void testSimple() {
        assertThat(strategy.getEntityClass()).isEqualTo(SimpleEntity.class);
    }

    @Test
    public void testPermission() {
        final Authentication anonymous = TestAuthenticationTokenUtil.createNotAuthenticated();
        final Authentication user = TestAuthenticationTokenUtil.createUserAuthentication();
        final Authentication admin = TestAuthenticationTokenUtil.createAdminAuthentication();

        assertThat(strategy.hasPermission(entity, EntityPermission.READ, anonymous)).isFalse();
        assertThat(strategy.hasPermission(entity, EntityPermission.READ, user)).isTrue();
        assertThat(strategy.hasPermission(entity, EntityPermission.UPDATE, anonymous)).isFalse();
        assertThat(strategy.hasPermission(entity, EntityPermission.UPDATE, user)).isFalse();
        assertThat(strategy.hasPermission(entity, EntityPermission.UPDATE, admin)).isTrue();
        assertThat(strategy.hasPermission(entity, EntityPermission.DELETE, admin)).isFalse();
        assertThat(strategy.hasPermission(entity, Permission.OTHER, anonymous)).isFalse();
        assertThat(strategy.hasPermission(entity, Permission.OTHER, user)).isTrue();
    }

    @Test
    public void testUnknownPermission() {
        final Authentication anonymous = TestAuthenticationTokenUtil.createNotAuthenticated();
        final Authentication user = TestAuthenticationTokenUtil.createUserAuthentication();
        final Authentication admin = TestAuthenticationTokenUtil.createAdminAuthentication();

        assertThat(strategy.hasPermission(entity, Permission.UNKNOWN, anonymous)).isFalse();
        assertThat(strategy.hasPermission(entity, Permission.UNKNOWN, user)).isFalse();
        assertThat(strategy.hasPermission(entity, Permission.UNKNOWN, admin)).isFalse();
    }

    @Test
    public void testRoleHierarchyApplied() {
        final Authentication userAuthentication = TestAuthenticationTokenUtil.createUserAuthentication();

        // Invoke
        assertThat(strategy.hasPermission(entity, EntityPermission.READ, userAuthentication)).isTrue();

        // Verify invocation
        Mockito.verify(roleHierarchy, Mockito.times(1)).getReachableGrantedAuthorities(
                Mockito.same(userAuthentication.getAuthorities()));
    }
}
