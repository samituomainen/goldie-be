Goldie React UI
===============

UI layer in React for Goldie BE.

#### TODO  

* Add e2e testing


Used technologies
-----------------

* react
* react-bootstrap
* react-router
* react-redux & redux
* redux-immutable
* redux-saga
* redux-forms
* redux-auth-wrapper
* react-router-role-authorization
* i18next & react-18next
* fixed-data-table
* axios

* webpack for building
* eslint for code quality
* mocha & enzyme for testing

Airbnb React/JSX Style Guide is used in the project. The file structure is feature-based and therefore doesn't follow the ordinary components, containers, actions, reducers & store approach because the chosen approach is easier to follow and more scalable to bigger projects.  


How to use
----------

Install Node and NPM to your computer, run `npm install` in the project root.

Run `npm start` to start the webpack development server. The website should be available at `http://localhost:3000`. 
React-hot-loader should reload changes automatically without refresh (when possible).

Run `npm build` to create a one time development build of the project. Results can be found in the `/build` folder.

Run `npm build:prod` to create a production ready, optimized build. Results can be found in the `/build` folder.
This command is run by the ant build script to build the frontend.


Contributions
-------------

If you have some great ideas that should be utilized in Goldie React UI, ask (Juho Sassali / Otto Kivikärki) or at the #react slack channel.
