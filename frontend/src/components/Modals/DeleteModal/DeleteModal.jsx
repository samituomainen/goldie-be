import React, { PropTypes } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { translate, Interpolate } from 'react-i18next';

/* eslint-disable */
const DeleteModal = ({ deleteTarget, modalData, show, onHide, onSuccess, t }) => (
  <Modal dialogClassName="deletemodal" show={show} onHide={onHide}>
    <Modal.Header closeButton>
      <Modal.Title>{t('deletemodal.title')}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <Interpolate i18nKey="modals:deletemodal.textContent" value={modalData.username} />
    </Modal.Body>
    <Modal.Footer>
      <Button bsStyle="danger" onClick={(data) => { deleteTarget(modalData.id, onSuccess) }}>{t('deletemodal.buttons.delete')}</Button>
      <Button onClick={onHide}>{t('deletemodal.buttons.cancel')}</Button>
    </Modal.Footer>
  </Modal>
);

DeleteModal.propTypes = {
  onHide: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  deleteTarget: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired,
  t: PropTypes.func.isRequired,
  modalData: PropTypes.object.isRequired
};

export default translate(['modals'], { wait: true })(DeleteModal);
