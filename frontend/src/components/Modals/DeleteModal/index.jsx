import { connect } from 'react-redux';
import { requestDeleteUser } from '../../../views/users/actions';

import DeleteModal from './DeleteModal';

const mapStateToProps = state => state.toJS();

const mapDispatchToProps = dispatch => ({
  deleteTarget(id, onSuccess) {
    dispatch(requestDeleteUser({
      id,
      onSuccess
    }));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeleteModal);
