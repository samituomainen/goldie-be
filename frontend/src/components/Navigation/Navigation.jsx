import React, { Component, PropTypes } from 'react';
import { LinkContainer } from 'react-router-bootstrap';
import { Glyphicon, MenuItem, Nav, Navbar, NavDropdown, NavItem } from 'react-bootstrap';
import { translate } from 'react-i18next';
import i18n from '../../i18n';

import RoleAwareNavigationItem from '../RoleAwareNavigationItem';

import './navigation.scss';

const styles = {
  brand: {
    cursor: 'pointer'
  }
};

class Navigation extends Component {

  constructor(props) { // eslint-disable-line
    super(props);
  }

  changeLanguage(newLanguage) {
    i18n.changeLanguage(newLanguage);
  }

  render() {
    const { isAuthenticated, logout, t } = this.props;

    return (
      <Navbar inverse staticTop>
        <Navbar.Header>
          <Navbar.Brand>
            <LinkContainer to="home" style={styles.brand}>
              <span><span className="glyphicon glyphicon-home">&nbsp;</span>{t('home')}</span>
            </LinkContainer>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <RoleAwareNavigationItem
              eventKey={1}
              text={t('menuItems.users')}
              to="/users"
              userRole={this.props.role}
              allowedRoles={['ROLE_ADMIN']}
            />
          </Nav>
          <Nav pullRight>
            <NavDropdown
              eventKey={2}
              title={<span><Glyphicon glyph="flag" /> {t('menuItems.languageMenu.title')}</span>}
              id="language-nav-dropdown"
            >
              <MenuItem
                eventKey={'fi'}
                onSelect={(newLanguage) => { this.changeLanguage(newLanguage) }}
              >
                {t('menuItems.languageMenu.items.fi')}
              </MenuItem>
              <MenuItem
                eventKey={'en'}
                onSelect={(newLanguage) => { this.changeLanguage(newLanguage) }}
              >
                {t('menuItems.languageMenu.items.en')}
              </MenuItem>
              <MenuItem
                eventKey={'sv'}
                onSelect={(newLanguage) => { this.changeLanguage(newLanguage) }}
              >
                {t('menuItems.languageMenu.items.sv')}
              </MenuItem>
            </NavDropdown>
            {isAuthenticated ?
              <NavItem eventKey={3} onClick={() => { logout() }}>
                <Glyphicon glyph="log-out" /> {t('menuItems.account.logout')}
              </NavItem>
              :
                <LinkContainer to="signin">
                  <NavItem eventKey={4}>
                    <Glyphicon glyph="log-in" /> {t('menuItems.account.login')}
                  </NavItem>
                </LinkContainer>
            }
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

Navigation.propTypes = {
  isAuthenticated: PropTypes.bool,
  t: PropTypes.func.isRequired,
  logout: PropTypes.func,
  role: PropTypes.string
};

export default translate(['menu'], { wait: true })(Navigation);
