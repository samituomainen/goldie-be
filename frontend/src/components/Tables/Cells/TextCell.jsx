import React, { PropTypes } from 'react';
import { Cell } from 'fixed-data-table';

const TextCell = ({ rowIndex, data, col, ...props }) => (
  <Cell {...props}>
    {data[rowIndex][col]}
  </Cell>
);

TextCell.propTypes = {
  rowIndex: PropTypes.number,
  col: PropTypes.string,
  data: PropTypes.array
};

export default TextCell;
