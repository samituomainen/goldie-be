import React, { PropTypes } from 'react';
import { Cell } from 'fixed-data-table';

import DeleteModalButton from '../../Modals/DeleteModal/DeleteModalButton';
import EditUserModalButton from '../../Modals/EditUserModal/EditUserModalButton';

const UserActionsButtonCell = ({ data, rowIndex, ...props }) => (
  <Cell {...props}>
    <DeleteModalButton modalData={data[rowIndex]} />
    <EditUserModalButton userData={data[rowIndex]} />
    <div style={{ clear: 'both' }} />
  </Cell>
);

UserActionsButtonCell.propTypes = {
  rowIndex: PropTypes.number,
  data: PropTypes.array
};

export default UserActionsButtonCell;
