import authentication from './authentication';
import users from './users';
import util from './util';
import setupHttpInterceptors from './interceptors';

setupHttpInterceptors();

const Api = {
  authentication,
  users,
  util
};

export default Api;
