import { call, put } from 'redux-saga/effects';

// For getting nested items from Immutable.js Map with dot notation
export const getWithDot = (map, path) => (
  map.getIn(path.split('.'))
);

export const createGenerator = (endpoint, successCallback, errorCallback) =>
  function* created({ payload }) {
    try {
      const response = yield call(endpoint, payload);
      yield put(successCallback(response.data));
    } catch (err) {
      yield put(errorCallback(err));
    }
  };
