/* global _ */
import createSagaMiddleware from 'redux-saga';
import createLogger from 'redux-logger';
import { createStore, compose, applyMiddleware } from 'redux';
import { Iterable } from 'immutable';
import { combineReducers } from 'redux-immutable';
import { reducer as formReducer } from 'redux-form/immutable';

import { loadState, saveState } from './util/localStorage';

import rootReducer from './reducers';
import rootSaga from './sagas';

/**
  Create a logger interface for development purposes. Displays state related
  information in the console.
**/
const createReduxLogger = () => {
  const stateTransformer = (state) => {
    if (Iterable.isIterable(state)) {
      return state.toJS();
    }
    return state;
  };

  return createLogger({
    stateTransformer
  });
};

/**
  Collect the state to save in the localStorage. Save authentication related
  information for now.
**/
const getStateForLocalStorage = (state) => {
  const authStateObj = state.getIn(['rootReducer', 'auth']).toJS();
  const filteredState = _.omitBy(authStateObj, (val, key) => key === 'loginFailedError');

  return {
    rootReducer: {
      auth: filteredState
    }
  };
};

/**
  Configure the redux store instance and add middlewares. Saves the state to
  localStorage on an interval of 1s aswell.
**/
const configureStore = () => {
  const persistedState = loadState();
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware];

  // If we are in development mode, use logger and transform immutable state to JS for clarity
  if (process.env.NODE_ENV === 'development') {
    middlewares.push(createReduxLogger());
  }

  const store = createStore(
    combineReducers({
      rootReducer,
      form: formReducer
    }),
    persistedState,
    compose(
      applyMiddleware(...middlewares),
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
  );

  store.subscribe(_.throttle(() => { // eslint-disable-line
    // Only save the relevant state to localStorage for reuse
    const stateForStorage = getStateForLocalStorage(store.getState());
    saveState(stateForStorage);
  }, 1000));

  sagaMiddleware.run(rootSaga);

  return store;
};

export default configureStore;
