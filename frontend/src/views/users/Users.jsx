import React, { Component, PropTypes } from 'react';
import Immutable from 'immutable';

import UsersTable from '../../components/Tables/UsersTable';

class Users extends Component {

  constructor(props) { // eslint-disable-line
    super(props);
  }

  componentDidMount() {
    const queryParams = {
      page: 0,
      size: 20,
      sort: 'username',
      type: 'page'
    };
    this.props.requestUserData(queryParams);
  }

  render() {
    const { users } = this.props;

    return (
      <div>
        <UsersTable
          users={users}
          width={this.props.containerWidth}
        />
      </div>
    );
  }
}

Users.propTypes = {
  containerWidth: PropTypes.number.isRequired,
  requestUserData: PropTypes.func.isRequired,
  users: PropTypes.instanceOf(Immutable.List)
};

export default Users;
