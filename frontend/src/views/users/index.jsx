import { connect } from 'react-redux';
import Dimensions from 'react-dimensions';
import Users from './Users';
import {
  requestUserData,
  requestSingleUser
} from './actions';

import './users.scss';

const mapStateToProps = state => ({
  users: state.getIn(['rootReducer', 'userData', 'users'])
});

const mapDispatchToProps = dispatch => ({
  requestUserData(queryParams) {
    dispatch(requestUserData(queryParams));
  },
  requestSingleUser(queryParams) {
    dispatch(requestSingleUser(queryParams));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dimensions()(Users));
