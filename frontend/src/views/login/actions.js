import { createAction } from 'redux-actions';

export const loginUser = createAction('LOGIN_USER');
export const logoutUser = createAction('LOGOUT_USER');

export const loginSuccess = createAction('LOGIN_SUCCESS');
export const logoutSuccess = createAction('LOGOUT_SUCCESS');

export const loginFailure = createAction('LOGIN_FAILURE');
export const logoutFailure = createAction('LOGOUT_FAILURE');

export const resetAuthenticationState = createAction('RESET_AUTHENTICATION_STATE');
export const redirectAndResetAuthentication = createAction('REDIRECT_AND_RESET_AUTHENTICATION');

export const authenticateUser = createAction('AUTHENTICATE_USER');

export const resetValidationState = createAction('RESET_LOGIN_VALIDATION_STATE');
