import React, { Component, PropTypes } from 'react';
import { translate } from 'react-i18next';

import LoginForm from '../../components/Forms/LoginForm';
import CenteredContent from '../../components/CenteredContent';
import ErrorMessage from '../../components/Messages/ErrorMessage';

class Login extends Component {

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    this.props.resetValidationState();
  }

  componentWillReceiveProps(nextProps) {
    const { isAuthenticated } = nextProps;
    const { isAuthenticated: wasAuthenticated } = this.props;

    if (!wasAuthenticated && isAuthenticated) {
      this.context.router.push('home');
    }
  }

  onSubmit(formData) {
    this.props.login(formData);
  }

  render() {
    const { t } = this.props;

    return (
      <CenteredContent>
        <ErrorMessage isVisible={this.props.error}>
          <strong>{t('validation.error_1')}</strong>
          <p>{t('validation.error_2')}</p>
        </ErrorMessage>
        <LoginForm onSubmit={this.onSubmit} />
      </CenteredContent>
    );
  }
}

Login.contextTypes = {
  router: React.PropTypes.object.isRequired
};

Login.propTypes = {
  login: PropTypes.func.isRequired,
  resetValidationState: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  error: PropTypes.bool,
  t: PropTypes.func.isRequired
};

export default translate(['login'], { wait: true })(Login);
export { Login as PureLogin };
