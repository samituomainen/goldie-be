/* eslint-env node, mocha */
/* eslint no-console: 0*/

import { expect } from 'chai';
import Immutable from 'immutable';

import { loginSuccess, loginFailure } from '../actions';
import verificationReducer from '../reducers';
// verificationReducer.__Rewire__('browserHistory', {
//   push: () => {
//
//   }
// });

describe('authentication reducer', () => {
  const initialState = {
    isAuthenticated: false,
    token: null,
    role: null,
    loginFailedError: false
  };

  it('should return the initial state correctly', () => {
    expect(
      verificationReducer(undefined, { type: '' }).equals(Immutable.Map(initialState))
    ).to.equal(true);
  });

  it('should handle LOGIN_SUCCESS', () => {
    const startingState = Immutable.Map(initialState);
    const expectedState = Immutable.Map({ isAuthenticated: true,
      role: 'ROLE_ADMIN',
      token: 'test',
      loginFailedError: false });
    const action = {
      role: 'ROLE_ADMIN'
    };

    expect(
      verificationReducer(startingState, loginSuccess(action)).equals(expectedState)
    ).to.equal(true);
  });

  it('should handle LOGIN_FAILURE', () => {
    const startingState = Immutable.Map(initialState);
    const expectedState = Immutable.Map(initialState).merge({
      loginFailedError: true
    });

    expect(
      verificationReducer(startingState, loginFailure()).equals(expectedState)
    ).to.equal(true);
  });
});
