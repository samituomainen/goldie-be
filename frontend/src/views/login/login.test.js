/*eslint-env node, mocha */
/*eslint no-console: 0*/
'use strict';

import React from 'react';
import { shallow, mount, render } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';

import ContextWrapper from '../../test/ContextWrapper';

import { PureLogin } from './Login';

describe('Login component', () => {
  let props = {};
  let component = null;

  beforeEach(() => {
    props = {
      login: sinon.spy(),
      resetValidationState: sinon.spy(),
      isAuthenticated: true,
      t: sinon.spy()
    };
    component = render(<ContextWrapper><PureLogin {...props} /></ContextWrapper>);
  });

  it('should call resetValidationState on init', () => {
    expect(props.resetValidationState.calledOnce).to.equal(true);
  });
});
