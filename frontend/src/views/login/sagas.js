import { takeEvery } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import { browserHistory } from 'react-router';
import { createGenerator } from '../../util/helpers';
import {
  loginUser,
  logoutUser,
  loginSuccess,
  logoutSuccess,
  authenticateUser,
  resetAuthenticationState,
  redirectAndResetAuthentication,
  loginFailure,
  logoutFailure
} from './actions';
import Api from '../../api';

export const login = createGenerator(
  Api.authentication.login, loginSuccess, loginFailure
);

export function* logout() {
  try {
    const response = yield call(Api.authentication.logout);
    yield put(logoutSuccess(response.data));
    browserHistory.push('signin');
    yield put(authenticateUser());
  } catch (err) {
    yield put(logoutFailure(err));
  }
}

export function* authenticate() {
  try {
    yield call(Api.authentication.authenticate);
  } catch (err) {
    yield put(resetAuthenticationState(err));
  }
}

export function* redirectAndReset() {
  yield put(resetAuthenticationState());
  browserHistory.push('signin');
}

const watchLoginUser = takeEvery(loginUser().type, login);
const watchLogoutUser = takeEvery(logoutUser().type, logout);
const watchAuthenticateUser = takeEvery(authenticateUser().type, authenticate);
const watchRedirectAndResetAuthentication = takeEvery(
  redirectAndResetAuthentication().type, redirectAndReset);

// Export as an array so it cleaner to handle in the root saga.
export default [
  watchLoginUser,
  watchLogoutUser,
  watchAuthenticateUser,
  watchRedirectAndResetAuthentication
];
