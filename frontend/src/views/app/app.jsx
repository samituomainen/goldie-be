import React, { Component, PropTypes } from 'react';
import { Grid } from 'react-bootstrap';

import Navigation from '../../components/Navigation';
import './app.scss';

class App extends Component {

  componentWillMount() {
    this.props.authenticate();
  }

  render() {
    return (
      <div className="app">
        <Navigation />
        <Grid className="fill">
          {this.props.children}
        </Grid>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.node.isRequired,
  authenticate: PropTypes.func.isRequired
};

export default App;
