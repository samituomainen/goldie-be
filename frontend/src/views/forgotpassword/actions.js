import { createAction } from 'redux-actions';

export const requestForgotPassword = createAction('REQUEST_FORGOT_PASSWORD');
export const forgotPasswordRequestSuccess = createAction('REQUEST_FORGOT_PASSWORD_SUCCESS');
export const forgotPasswordRequestFailure = createAction('REQUEST_FORGOT_PASSWORD_FAILURE');
export const resetValidationState = createAction('RESET_PASSWORD_VALIDATION_STATE');
