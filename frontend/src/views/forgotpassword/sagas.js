import { takeEvery } from 'redux-saga';
import { createGenerator } from '../../util/helpers';
import { requestForgotPassword, forgotPasswordRequestSuccess,
  forgotPasswordRequestFailure } from './actions';
import Api from '../../api';

export const requestPassword = createGenerator(
  Api.util.fetchForgottenPassword, forgotPasswordRequestSuccess, forgotPasswordRequestFailure
);

const watchRequestForgottenPassword = takeEvery(
  requestForgotPassword().type, requestPassword);

// Export as an array so it cleaner to handle in the root saga.
export default [
  watchRequestForgottenPassword
];
